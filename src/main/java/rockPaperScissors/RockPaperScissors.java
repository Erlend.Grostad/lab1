package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // start game loop
        while(true) {
            // play game once and determine round result
            String result = this.round();
            if (result.equals("win")) {
            humanScore++;
            }
            else if (result.equals("lose")) {
                computerScore++;
            }
            // Print scores after completed round and ask if user wants to keep playing
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            String cont = this.readInput("Do you wish to continue playing? (y/n)?");
            if (cont.equals("n") || cont.equals("no")) {
                System.out.println("Bye bye :)");
                break;
            }
        }              
    }

    public String round() {
        // Starts round 1 and prompts user for input
        System.out.println("Let's play round "+roundCounter);
        roundCounter++;               
        // Iterates through the rps list and matches it with the given choice
        while(true){
            String choice = this.readInput("Your choice (Rock/Paper/Scissors)?");
            for (int i = 0; i < rpsChoices.size(); i++)
            if(rpsChoices.get(i).equals(choice)) {
                // if the choice matches with one of the strings in the list, choose random computer move
                double index = Math.floor(Math.random() * rpsChoices.size());
                int indexint = (int) index;
                String botchoice = rpsChoices.get(indexint);
                
                // Determine winner
                return this.rpsresult(choice, botchoice);
            }
            // If the choice does not match, prompt again
            System.out.println("I do not understand " + choice + ". Could you try again?");
        }          
    }

    public String rpsresult(String human, String computer) {
        // Handles win or lose logic
         if(human.equals("rock")) {
            if(computer.equals("paper")){
                System.out.println("Human chose rock, computer chose paper. Computer wins!");
                return "lose";
            }
            if(computer.equals("scissors")){
                System.out.println("Human chose rock, computer chose scissors. Human wins!");
                return "win";
            }
            else {
                System.out.println("Human chose rock, computer chose rock. It's a tie!");
                return "draw";
            }
            
        }
        else if(human.equals("paper")) {
            if(computer.equals("scissors")){
                System.out.println("Human chose paper, computer chose scissors. Computer wins!");
                return "lose";
            }
            if(computer.equals("rock")){
                System.out.println("Human chose paper, computer chose rock. Human wins!");
                return "win";
            }
            else {
                System.out.println("Human chose paper, computer chose paper. It's a tie!");
                return "draw";
            }
        }
        else {
            if(computer.equals("rock")){
                System.out.println("Human chose scissors, computer chose rock. Computer wins!");
                return "lose";
            }
            if(computer.equals("paper")){
                System.out.println("Human chose scissors, computer chose paper. Human wins!");
                return "win";
            }
            else {
                System.out.println("Human chose scissors, computer chose scissors. It's a tie!");
                return "draw";
            }
        }
    }
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
